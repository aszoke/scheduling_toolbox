function [sl lm] = edd1(p,d)
%EDD1 earliest due date algorithm for the l||Lmax problem 
%
% Idea: schedule the job that is closest to being late: order the jobs by 
% nondecreasing due dates (breaking ties arbitrarily) and schedule in that order
%
% Theorem(Jac55): EDD1 is an exact algorithm for l||Lmax
%
% Syntax:
%     [sl lm] = edd1(p,d) 
%   Input params:
%      p             - processing times
%      d             - due dates
%   Return values:
%      sl            - schedule
%      lm            - Lmax -- sum(Cj-dj)
%
% Complexity: ?
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% length checking
if length(p) ~= length(d)
    error('Length of processing time vector must be equal to due date vector!');
end;

% -- function body --

% sorting due dates into ascending order
[dsorted,ndx] = sort(d,'ascend');

% sorting processing times (jobs) according to due dates
sl = p(ndx);

% computing Cj (completion time of job j)
c = zeros(1,length(p));
c(1) = sl(1);
for j=2:length(p)
    c(j) = c(j-1) + sl(j);
end;

% compute maximum lateness (sum(Cj-dj))
diff = c-dsorted;
% find those who late
lateness = find(diff > 0);
lm = sum(lateness);

end

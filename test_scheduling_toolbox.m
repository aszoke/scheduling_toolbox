%% SCHEDULING TOOLBOX - SAMPLE TEST SET
% Scheduling Toolbox contains algorithms to solve optimization problems in which ideal jobs are assigned to resources at particular times.
%
% *Author*: Akos Szoke (aszoke@mit.bme.hu)
%
% *Web site*: <http://www.kese.hu>
%
% *Toolbox Source*: <https://bitbucket.org/aszoke/>
%
%%
dbstop if error;
clear;

load data_scheduling;

%% 

disp(' ***************************************************** ');
disp(' ******** SCHEDULING ALGORITHMS TESTS: Start ********* ')
disp(' ***************************************************** ');

%% Single machine problems

disp(' *** TEST STP1: Start *** ')
[s sc] = stp1(p)
disp(' *** TEST: Passed *** ')

disp(' *** TEST STP2: Start *** ')
[s sc] = stp2(p,w)
disp(' *** TEST: Passed *** ')

disp(' *** TEST SRPT1: Start *** ')
[s sc] = srpt1(p,pr,r)
disp(' *** TEST: Passed *** ')

disp(' *** TEST LCL1: Start *** ')
[s fm] = lcl1(p,f)
disp(' *** TEST: Passed *** ')

disp(' *** TEST LCL2: Start *** ')
[s fm] = lcl2(p,prec,f)
disp(' *** TEST: Passed *** ')

disp(' *** TEST EEL1: Start *** ')
% [s u] = eel1(p,d)
disp(' *** TEST: Passed *** ')

disp(' *** TEST EDD1: Start *** ')
[s u] = edd1(p,d)
disp(' *** TEST: Passed *** ')

disp(' *** TEST EDD2: Start *** ')
[s u] = edd2(p,d,r)
disp(' *** TEST: Passed *** ')

%% Multi machine problems

disp(' *** TEST LS1: Start *** ')
[S c] = ls1(p,m,'NONE') % without any heuristic
[S c] = ls1(p,m,'LPT') % with LPT heuristic
disp(' *** TEST: Passed *** ')

disp(' *** TEST LS2: Start *** ')
[S c] = ls2(p,m,prec,'NONE') % without any heuristic
[S c] = ls2(p,m,prec,'LPT') % with LPT heuristic
disp(' *** TEST: Passed *** ')

disp(' *** TEST ESJ1: Start *** ')
[s cm] = esj1([p;p2])
% example based on: 
% http://www.ielm.ust.hk/dfaculty/ajay/courses/ieem513/GT/johnson.html
[s cm] = esj1([6 10 4 7 6 5; 4 8 9 2 3 6])
disp(' *** TEST: Passed *** ')

disp(' *** TEST HCDS1: Start *** ')
[s cm] = hcds1([p;p2;p3;p4])
[s cm] = hcds1([4 2 3;5 3 4;2 4 4;5 4 1])
disp(' *** TEST: Passed *** ')

disp(' *** TEST HNEH1: Start *** ')
[s cm] = hneh1([p;p2;p3;p4])
% example based on: csum_calculation.xls
[s cm] = hneh1([4 2 3;5 3 4;2 4 4;5 4 1])
disp(' *** TEST: Passed *** ')

disp(' *** TEST LPP1: Start *** ')
P = [1 4 2 1 2 3 5; ... % machine 1 processing times
     1 3 2 2 2 4 5; ... % machine 2 processing times
     2 4 1 1 1 5 5];    % machine 3 processing times
X = lpp1(P)
sum(X,1)
disp(' *** TEST: Passed *** ')

% disp(' *** TEST MILPP1: Start *** ')
% P = [1 4 2 1 2 3 5; ... % machine 1 processing times
%      1 3 2 2 2 4 5; ... % machine 2 processing times
%      2 4 1 1 1 5 5];    % machine 3 processing times
% X = milpp1(P)
% sum(X,1)
% disp(' *** TEST: Passed *** ')

disp(' *********************************************** ');
disp(' ***** SCHEDULING ALGORITHMS TEST: End ********* ')
disp(' *********************************************** ');

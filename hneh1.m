function [sl cmax] = hneh1(pn)
%HNEH1 Nawaz, Enscore and Ham's heuristic for the F|prmu|Cmax problem 
% prmu: (permutation) indicates that the operations occur on the machines
% in the same order
%
% Syntax:
%     [sl cmax] = hneh1(pn) 
%   Input params:
%      pn            - processing times on machine 1 ... n
%   Return values:
%      sl            - schedule
%      cmax          - maximum completion time
%
% Complexity: ?
% Space     : ?
%
% Reference:
% (Tkindt:mul06)
%   T'kindt et. al. Multicriteria Scheduling: Theory, Models and Algorithms, 
%   Springer, 2006,
%   Chapter 1, Nawaz, Enscore and Ham's heuristic for the F2|prmu|Cmax problem
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% Copyright 2006-2008

% -- input checking -- 

% -- function body --

sl = []; % 'sl' means 'scheduled list'

m = size(pn,1); % number of machines
n = size(pn,2); % number of jobs

% total processing times for jobs
psum = sum(pn,1);       % total operating time per machine
[b,ndx] = sort(psum,'descend');   % sorting columns in descending order
tp = pn(:,ndx);         % machines in decreasing total operating time

k = 2;
for i=1:n-1
    cms = zeros(k,1); % cmax values for each combination
    for j=1:k % number of comparisons (k column orders)
        if j==1
            tp2 = horzcat(tp(:,k),tp(:,1:k-1)); % insert into the 1st column
        elseif j==k
            tp2 = horzcat(tp(:,1:k-1),tp(:,k)); % insert into the last column
        else
            tp2 = horzcat(tp(:,1:j-1),tp(:,k),tp(:,j:k-1)); % insert into the middle somewhere
        end;
        cms(j)= cmaxcalc(tp2); % calculating cmax value for a given order of column
    end;
        [c,i] = min(cms); % selecting the min cmax arrangement
        % arranging the processing time matrix
        if i==1; % no rearrangement is necessary
        elseif i==k
            tp = horzcat(tp(:,1:k-1),tp(:,k),tp(:,k+1:n)); % insert into the last column
        else
            tp = horzcat(tp(:,1:i-1),tp(:,k),tp(:,i+1:n)); % insert into the middle somewhere
        end;
        k = k+1; % increasing comparisons
end;

sl = tp;
cmax = cmaxcalc(tp);
    
end

function cm = cmaxcalc(pn)
% cmax calcuates the maximum completion time of m machines, n operations
% problems. For illustration of the algorithm see 'csum_calculation.xls'

m = size(pn,1); % number of machines
n = size(pn,2); % number of operations (a job is made up m operations!)

% adding extra row to process time matrix to make the computation easier
pn2 = [zeros(1,n);pn];

% -- calculating starting time of operations
% m+1 contains one extra row in the first position
start = zeros(m+1,n);
for i=2:m+1
    for j=1:n
        if j==1
            start(i,j)=start(i-1,j)+pn2(i-1,j);
        else
            start(i,j)=max(start(i,j-1)+pn2(i,j-1),start(i-1,j)+pn2(i-1,j));
        end;
    end;
end;

% adding the last element duration
cm = start(m+1,n) + pn2(m+1,n);

end

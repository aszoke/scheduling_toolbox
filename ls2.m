function [SL cmax] = ls2(p,m,prec,schedstrategy)
%LS2 list scheduling with different strategies algorithm for the P|prec|Cmax problem 
%
% Idea: list scheduling (LS) algorithm is a generic greedy algorithm: whenever 
% a machine becomes available, process any unprocessed job
% (In contrast to P|pmtn|Cmax, P|prec|Cmax is NP-hard!)
%
% CMAX: is the lower bound approximation, namely:
% 1) CMAX >= sum(pj/m) 
% 2) CMAX >= pj for all jobs j
%
% Theorem (Gra66) LS (without any heuristic) is a 2-approximation algorithm for P|prec|Cmax.
% (i.e. Cmax  <= 2*CMAX)
%
% Syntax:
%     [SL cmax] = ls1(p,m,prec,schedstrategy) 
%   Input params:
%      p             - processing times
%      m             - machine number
%      prec          - precedence constraints (matrix)
%      schedstrategy - heuristics for the schedule (since it is an approx. algorithm)
%   Return values:
%      SL            - schedule
%      cmax          - Cmax
%
% Complexity: 
%   without any heuristic:  : Ordo(n + m)
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% precedence checking
if (size(prec,1) ~= length(p)) || (size(prec,2) ~= length(p))
    error('Size of precedence matrix is not consistent with processing time vector!');
end;

if ~isDAG(prec)
    error('Precedece matrix is not a simple (loop free, single connected) DAG!');
end;

% -- function body --

rlist = []; % 'rlist' means 'ready list'
SList = []; % 'SList' means 'scheduled list'

% list scheduling
SL = zeros(m,length(p));  % machine x jobs
for j=1:length(p)   % number of jobs
    % -- actualizing the 'ready list'
    % find potentially schedulable items according to updated 'prec'
    % sum(prec): summing columns -> where it equals to '0' it doesn't have precedessor
    pot = find(sum(prec,2) == 0);
    % updating 'rs': {potential items} - {scheduled items}
    rlist = setdiff(pot,SList);  
    
    if isempty(rlist)
        str = strcat('Infeasible problem! There is no schedulable item at step: ',int2str(j));
        error(str);
    end;
    
    switch upper(schedstrategy)
        case 'NONE'
            jobndx = rlist(1); % select the first element of rlist to schedule
        case 'LPT'
            [val,i] = max(p(rlist));
            jobndx = rlist(i);
        case 'SPT'
            [val,i] = min(p(rlist));
            jobndx = rlist(i);  
        otherwise
            error('Unknown strategy!');
    end;      
    
    [minmach,machndx] = min(sum(SL,2));     % find the minimally loaded machine
    posndx = min(find(SL(machndx,:) == 0)); % find the min uncheduled position
    SL(machndx,posndx) = p(jobndx);         % schedule on machine on position
    
    SList = vertcat(SList,jobndx);    % update 'scheduled list'
    
   % deleting scheduled item from 'prec', since it doesn't restrict the
   % precedented element(s) (if there is any precedented) 
   prec(:,jobndx) = 0;
end;

% calculating makespan (Cmax)
cmax = max(sum(SL,2));

end

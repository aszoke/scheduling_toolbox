function [sl u] = ejm1(p, dd)
%EJM1 Moore's algorithm for the l|di|f problem 
%
% Syntax:
%     [sl u] = ejm1(p, dd) 
%   Input params:
%      p            - processing times
%      dd           - due dates
%   Return values:
%      sl            - schedule
%      u             - number of late jobs
%
% Complexity: Ordo(n)
% Space     : ?
%
% Reference:
% (Tkindt:mul06)
%   T'kindt et. al. Multicriteria Scheduling: Theory, Models and Algorithms, 
%   Springer, 2006,
%   Chapter 1, Moore's algorithm for the l|di|f| problem
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% length checking
if length(p) ~= length(dd)
    error('Length of processing time vector must be equal to due date vector!');
end;

% -- function body --

tl = []; % 'tl' means 'tardy list'
sl = []; % 'sl' means 'scheduled list'

% ordering jobs according to EDD (earliest due dates)
[b,ix] = sort(dd);
slo = p(ix);

t = tardiness(slo,dd); % computing tardiness of each job

while ~isempty(find(t > 0)) % while there is tardy jobs
    % find tardy jobs. All jobs before k(1) are not tardy jobs
    k = find(t > 0); 
    % select the max processing time job (j) that precedes or equal with
    % the first tardy job
    [v,j] = max(slo(1:k(1))); 
    
    tl = horzcat(tl,slo(j));      % add max element to the tardy list 
    slo = slo(setdiff(1:length(slo), j)); % delete the max element
    dd = dd(setdiff(1:length(dd), j));    % delete the max element due date
    t = tardiness(slo,dd);        % recompute tardiness
end;    

sl = horzcat(slo,tl);   % concatenate not tardy and tardy jobs
u = length(tl);         % number of late jobs

end

function t = tardiness(slo,dd)
    % completion time of jobs
    c = zeros(length(slo),1)';
    c(1) = slo(1);
    for i = 2:length(slo)
        c(i) = c(i-1) + slo(i);
    end;
    % tardiness of jobs (positive means tardy job)
    t = c - dd;
end
function X = milpp1(P)
%MILPP1 mixed integer linear programming for planning of the R||Cmax problem (relaxed version of LPP1)
%
% Idea: It yields assignments to resources, but does not produce schedule 
%       (operation ordering within a resource): JUST simply assign tasks to resources!
%
% min d                                                     -- minimize makespan
% s.t.
% (1) sum_i(x_{i,j})            = 1         : j = 1,2,...,n -- each job must be fully processed
% (2) sum_j(p_{i,j}*x_{i,j})   <= d         : i = 1,2,...,m -- process time on each machine no more than d time
% (4')x_{i,j}                   = {0;1}     : j = 1,2,...,n and i = 1,2,...,m -- a job assigned to any machine must be non-negative
%
% where: x_{i,j} (X) -- denotes the fraction of job j that is processed on machine i
%        p_{i,j} (P) -- amount of processing that job j would require, if run entirely on machine i
%
% Syntax:
%     X = milpp1(p) 
%   Input params:
%      P             - amount of processing that job j would require, if run entirely on machine i
%   Return values:
%      X            - allocated operations (part of a job) to resources
%
% Complexity: 
%   without any heuristic:  : ?
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: LPP1

% Copyright 2006-2008

% -- input checking -- 

% -- function body --

n = size(P,2); % job number
m = size(P,1); % machine number

% maximum time to complete (optimal solution is less than the max processing time on machines)
d = max(sum(P,2));  % makespan

% -- transforming e.q. (1) to form linear equations for linprog
% produce the first coefficient matrix 

% ----------------------------------------------
% Form of the target Aeq matrix (n,nm)
%
% --m--|--m--|--....|--m--|
% 1 . 1 0 . 0 ............ = 1
% 0 . 0 1 . 1 0 0 ........ = 1
%          .......
% .................0 1 . 1 = 1
% ----------------------------------------------

A13right = zeros(n,n*m);
for i=1:n*m
    j = ceil(i/m);
    A13right(j,i)=1;
end;
Aeq = A13right;

% produce the right side of the equation 1
beq = ones(n,1);


% -- transforming e.q. (2) to form linear equations for linprog
% produce the left matrix of the second coefficient matrix 
% mutiply vertically m times

% ----------------------------------------------
% Form of the target A2 matrix (m,nm) 
%
% --------m------|-------m--------|....|----------m----|
% p(1,1) 0..... 0 p(1,m+1)  0....0 .....p(1,2m+1) 0...0 <= d
% 0 p(2,1) 0....0 0 p(2,m+1)  0..0 .....0 p(2,2m+1) 0.0 <= d
%                             .......
% 0 ...... 0p(m,1) 0... 0 p(m,m+1) .... 0...0 p(2,2m+1) <= d
% ----------------------------------------------

% transform matrix P to vector
tmp = P';
p = tmp(:)'; 

Aleft = p;
for i=2:m
    Aleft = vertcat(Aleft,p);
end;
% produce the right matrix
Aright = zeros(m,n*m);
for h=1:m
    for i=1:n
        for j=1:m
            if j == 1
                Aright(h,(h+(i-1)*m))=1;
            end;
        end;
    end;
end;
A = Aleft .* Aright;

% produce the right side of the equation 1
b = ones(m,1)*d; 

% -- calculating the constructed mixed-integer-linear program
%   Input params:
%     f         - Linear objective function vector f
%     A         - Matrix for linear inequality constraints
%     b         - Vector for linear inequality constraints
%     Aeq       - Matrix for linear equality constraints
%     beq       - Vector for linear equality constraints
%     lb        - Vector of lower bounds
%     ub        - Vector of upper bounds
%     ndxi      - Vector of indeces of integer variables
%     e         - Integarilty tolerance

f  = -1*ones(n*m,1)';   % multiply with (-1) since we would like to minimize
lb = zeros(n*m,1)';         
ub = (ones(n*m,1) .* inf)';
ndxi = 1:n*m;             % all the variables are integer
e=2^-24;

[x v exitflag]= milprog(f,A,b,Aeq,beq,lb,ub,ndxi,e);

if exitflag <= 0
    error('No feasible solution!');
end

% -- constructing the scheduling matrix X  
for i=1:n
    for j=1:m
        X(j,i) = x(m*(i-1)+j);
    end;
end;

end

function [sl sc] = stp1(p)
%STP1 shortest processing time algorithm for the l||sum(Cj) problem 
%
% Idea: order the jobs by nondecreasing processing time (breaking ties arbitrarily) 
% and schedule in that order
%
% Theorem: STP1 is an exact algorithm for sum(Cj)
%
% Syntax:
%     [sl sc] = stp1(p) 
%   Input params:
%      p             - processing times
%   Return values:
%      sl            - schedule
%      sc            - sum(Cj) -- minsum 
%
% Complexity: ?
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% -- function body --

% sorting processing times into ascending order
sl = sort(p,'ascend');

% compute sum of completion times
sc = sum(sl(1:length(sl)));

end

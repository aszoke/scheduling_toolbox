function [sl fm] = lcl1(p,f)
%LCL1 least cost last algorithm for the l||fmax problem 
%
% Idea: schedule the job that is closest to being late: order the jobs by 
% nondecreasing due dates (breaking ties arbitrarily) and schedule in that order
%
% Syntax:
%     [sl fm] = lcl1(p,f) 
%   Input params:
%      p             - processing times
%      f             - penalty functions on each job
%   Return values:
%      sl            - schedule
%      fm            - max fj(Cj)
%
% Complexity: Ordo(n^2)
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% length checking
if (length(p) ~= size(f,2)) || (length(p) ~= size(f,1))
    error('Length of processing time vector must be equal to numbers of penalty functions!');
end;

% -- function body --

fm = 0;
sl = [];
% find job j that minimizes fj(p(J)) /least cost/ 
for j=length(p):-1:1
    [lc,ndx] = min(f(:,j));         % select least cost...
    sl = vertcat(lc,sl);            % ...and schedule it last
    fm = fm + f(ndx,j);             % fmax
    f(ndx,:) = [];                  % delete scheduled item OR an other method:
    % f(ndx,:) = inf;               % scheduled item are practically eliminated
end;
    
end

# Scheduling Toolbox

## About

Scheduling Toolbox contains basic algorithms to solve optimization problems in which jobs are assigned to resources at particular times.

**Author**: Akos Szoke <aszoke@mit.bme.hu>

**Web site**: [http://www.kese.hu](http://www.kese.hu)

**Toolbox Source**: [https://bitbucket.org/aszoke/](https://bitbucket.org/aszoke/)

### Features

**Greedy algorithms**

* **edd1** - earliest due date algorithm for the l||Lmax problem 
* **edd2** - earliest due date algorithm for the l|rj,pmtn|Lmax problem 
* **srpt1** - SRTP1 shortest remaining proceing time algorithm for the l|rj,pmtn|sum(Cj) problem 
* **stp1** - shortest proceing time algorithm for the l||sum(Cj) problem 
* **stp2** - shortest proceing time with weights algorithm for the l||sum(wjCj) problem 
* **ls1** - list scheduling with different strategies algorithm for the P||Cmax problem 
* **ls2** - list scheduling with different strategies algorithm for the P|prec|Cmax problem 

**Sophisticated greedy algorithms**

* **lcl1** - least cost last algorithm for the l||fmax problem 
* **lcl2** - least cost last algorithm for the l|prec|fmax problem 
* **ejm1** - Moore's algorithm for the l|di|f problem 

**Linear programming algorithms**

* **lpp1** - linear programming for planning (allocation of resources) of the R|pmtn|Cmax problem 
* **milpp1** - mixed integer linear programming for planning of the R||Cmax problem (relaxed version of LPP1) 

**Misc algorithms (T'Kindt: Multicriteria Scheduling)**

* **eel1** - Lawler's algorithm for the l|prec|fmax problem 
* **esj1** - Johnson's algorithm for the F2|prmu|Cmax problem 
* **hcds1** - Campbell, Dudek and Smith's heuristic for the F|prmu|Cmax problem 
* **hneh1** - Nawaz, Enscore and Ham's heuristic for the F|prmu|Cmax problem 

* **lscap** - list scheduling with different strategies algorithm for the P|prec|Cmax problem 

### Dependencies

Linprog Toolbox - only for [milpp1.m](./scheduling_toolbox/src/master/milpp1.m)

## Usage

using the functions in the feature list

### Example

[test_scheduling_toolbox.m](./scheduling_toolbox/src/master/test_scheduling_toolbox.m)

The generated log of the example can be found in
[test_scheduling_toolbox.html](https://bitbucket.org/aszoke/scheduling_toolbox/raw/master/html/test_scheduling_toolbox.html).

**Note**: *Due to the Bitbucket HTML preview restriction (see [HTML rendering for generated doc](https://bitbucket.org/site/master/issue/6353/html-rendering-for-generated-doc)), in order to view the Test run samples in HTML, use should use the [GitHub & BitBucket HTML Preview](http://htmlpreview.github.io/) service. (Copy the link below and paste into the text box that can be found in the service.)*

### Test data

The test data can be generated with [generate_data.m](./scheduling_toolbox/src/master/generate_data.m)

* [data_scheduling.mat](./scheduling_toolbox/src/master/data_scheduling.mat)

see them in the example

## License

(The MIT License)

Copyright (c) 2011 Akos Szoke

Permiion is hereby granted, free of charge, to any person obtaining a copy of this software and aociated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permiion notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRE OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNE FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


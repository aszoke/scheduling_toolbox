function [sl lm] = edd2(p,d,r)
%EDD2 earliest due date algorithm for the l|rj,pmtn|Lmax problem 
%
% Idea: schedule the job that is closest to being late: order the jobs by 
% nondecreasing due dates (breaking ties arbitrarily) and schedule in that order
%
% Theorem (Hor74): SRPT is an exact algorithm for l|rj,pmtn|Lmax 
%
% Syntax:
%     [sl lm] = edd1(p,d) 
%   Input params:
%      p             - processing times
%      d             - due dates
%      r             - release time of jobs
%   Return values:
%      sl            - schedule
%      lm            - Lmax (min maximum lateness)-- sum(Cj-dj)
%
% Complexity: ?
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% length checking
if length(p) ~= length(d)
    error('Length of processing time vector must be equal to due date vector!');
elseif length(d) ~= length(r)
    error('Length of due date vector must be equal to release time vector!');    
end;

% -- function body --

% sorting release times into ascending order
[rsorted,ndx] = sort(r,'ascend');

% sorting according to the release times order
psorted = p(ndx);
dsorted = d(ndx);

% note: less due date have to be scheduled earlier!
sl = zeros(1,length(p));
dd = zeros(1,length(p));
for j=1:length(p)
    minrel = min(rsorted);                  % min release value
    [tmp,col] = find(rsorted == minrel);    % indeces with min release value
    [tmp,ndx] = min(dsorted(col));    % min due date job with min release value
    sl(j) = psorted(col(ndx));       % select item in the schedule list
    dd(j) = dsorted(col(ndx));       % actualizing due date list as well
    dsorted(col(ndx)) = inf;    % to eliminate choosing the already selected job j
    rsorted(col(ndx)) = inf;    % detto
end;

% computing Cj (completion time of job j)
c = zeros(1,length(p));
c(1) = sl(1);
for j=2:length(p)
    c(j) = c(j-1) + sl(j);
end;

% compute maximum lateness (sum(Cj-dj))
diff = c-dd;
% find those who late
lateness = find(diff > 0);
lm = sum(lateness);

end

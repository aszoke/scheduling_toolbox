function [sl swc] = stp2(p,w)
%STP2 shortest processing time with weights algorithm for the l||sum(wjCj) problem 
% Theorem (Smith56): Scheduling jobs in nonincreasing order of wj/pj gives an optimal
% schedule for 1||sum(wjCj)
%
% Theorem: STP2 is an exact algorithm for sum(wjCj)
%
% Idea: scheduling jobs in nonincreasing order of wj/pj 
%
% Syntax:
%     [sl swc] = stp2(p,w) 
%   Input params:
%      p             - processing times
%      w             - weights of jobs
%   Return values:
%      sl            - schedule
%      swc            - sum(wjCj) -- weighted minsum 
%
% Complexity: ?
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% length checking
if length(p) ~= length(w)
    error('Length of processing time vector must be equal to weight vector!');
end;

% -- function body --

% sorting (processing times / weights) into ascending order
% Note:
% - more processing times more backwards in the list
% - more weights more forwards in the list
% so we construct p/w quotient
[tmp,ndx] = sort(p./w,'ascend');

% sorting according to the quotients (p/w)
sl = p(ndx);

% compute sum of completion times
swc = sum(sl(1:length(sl)));

end

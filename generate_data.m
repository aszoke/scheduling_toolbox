
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    DATA FILE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('---=== Data save procedure started ===---.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SCHEDULING DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
% PROCESSING TIMES
p  = [1 4 2 1 2 3 5];     % jobs
p2 = [2 5 3 1 2 4 1];
p3 = [3 2 4 1 5 4 3];
p4 = [1 5 4 3 1 1 1];

% WEIGHTS
w  = [1 2 1 3 1.5 2.1 3];     % weights of jobs

% -- DUE DATES
d = [1 2 5 9 15 12 11];

% RELEASE TIMES
r = [1 2 1 3 5 7 3];     % release time of jobs

% PRIORITIES
pr = [1 3 1 3 2 1 2];     % priorities of jobs

% MACHINES
m = 3;

% -- PRECEDENCES
% prec = zeros(length(p)); % no precedence relations between elements
prec = full(sparse([1 2 5],[2 3 6],ones(1,3),length(p),length(p)));

% -- COST FUNCTIONS: must be an increasing function of the completion time
% completion time
c(1) = p(1);
for i=2:length(p)
    c(i) = c(i-1) + p(i);
end;
% cost function
for i=1:length(p)
    f(i,:) = c * (1+0.1*i); % each job has a nondecreasing penalty function
end;

save data_scheduling;

disp('Saved: SCHEDULING DATA.');

disp('---=== Data save procedure ended ===---.');
% --- EOF ---

function [SL cmax] = ls1(p,m,schedstrategy)
%LS1 list scheduling with different strategies algorithm for the P||Cmax problem 
%
% Idea: list scheduling (LS) algorithm is a generic greedy algorithm: whenever 
% a machine becomes available, process any unprocessed job
% (In contrast to P|pmtn|Cmax, P||Cmax is NP-hard!)
%
% CMAX: is the lower bound approximation, namely:
% 1) CMAX >= sum(pj/m) 
% 2) CMAX >= pj for all jobs j
%
% Theorem (Gra66) LS (without any heuristic) is a 2-approximation algorithm for P||Cmax.
% (i.e. Cmax  <= 2*CMAX)
%
% Theorem (Gra69) LPT is a 4/3-approximation algorithm for P||Cmax
% (i.e. Cmax <= 4/3*CMAX) -> so it is better than without any heuristic
%
% Syntax:
%     [SL cmax] = ls1(p,m,schedstrategy) 
%   Input params:
%      p             - processing times
%      m             - machine number
%      schedstrategy - heuristics for the schedule (since it is an approx. algorithm)
%   Return values:
%      SL            - schedule
%      cmax          - Cmax
%
% Complexity: 
%   without any heuristic:  : Ordo(n + m)
%   with LPT heuristic      : O(m+n log n)
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% -- function body --

switch upper(schedstrategy)
    case 'NONE'
        psorted = p;
    case 'LPT'
        psorted = sort(p,'descend');
    case 'SPT'
        psorted = sort(p,'ascend');    
    otherwise
        error('Unknown strategy!');
end;

% list scheduling
SL = zeros(m,length(p));  % machine x jobs
for j=1:length(p)   % number of jobs
    [minmach,machndx] = min(sum(SL,2));     % find the minimally loaded machine
    posndx = min(find(SL(machndx,:) == 0)); % find the min uncheduled position
    SL(machndx,posndx) = psorted(j);        % schedule on machine on position
end;

% calculating makespan (Cmax)
cmax = max(sum(SL,2));

end

function [sl cmax] = hcds1(pn)
%HCDS1 Campbell, Dudek and Smith's heuristic for the F|prmu|Cmax problem 
% prmu: (permutation) indicates that the operations occur on the machines in the same order
%
% It is based on the ESJ1 Johnson's algorithm for the F2|prmu|Cmax problem
%
% Syntax:
%     [sl cmax] = hcds1(pn) 
%   Input params:
%      pn            - processing times on machine 1 ... n
%   Return values:
%      sl            - schedule
%      cmax          - maximum completion time
%
% Complexity: Ordo(n*log(n))
% Space     : ?
%
% Reference:
% (Tkindt:mul06)
%   T'kindt et. al. Multicriteria Scheduling: Theory, Models and Algorithms, 
%   Springer, 2006,
%   Chapter 1, Campbell, Dudek and Smith's heuristic for the F2|prmu|Cmax problem
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: esj1.m

% Copyright 2006-2008

% -- input checking -- 

% -- function body --

sl = []; % 'sl' means 'scheduled list'

m = size(pn,1); % number of machines
n = size(pn,2); % number of jobs (an job is made up m operations!)

sset = cell(m-1,1);
cmset = zeros(m-1,1);

for j=1:m-1
    % reducing the m machines problem to 2 machine centers problem
    p1 = sum(pn(1:j,:),1);  % constructig job process time on MC1 (machine center)
    p2 = sum(pn(j+1:m,:),1);    % constructig job process time on MC2
    
    [s2 cm2] = esj1([p1;p2]);   % calling Johnson's algorithm
    
    sset{j} = s2;         % put into the schedule set
    cmset(j) = cm2;       % put cmax into the cmax set
end;

[cmax,ndx] = min(cmset);    % select the minimal cmax value
sl = sset{ndx};   % select the optimal schedule
    
end

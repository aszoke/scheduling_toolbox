function sl = eel1(p, prec, fc)
%EEL1 Lawler's algorithm for the l|prec|fmax problem 
% fc (cost function): must be an increasing function of the completion time
%
% Syntax:
%     sl = eel1(p, prec, f) 
%   Input params:
%      p            - processing times
%      prec         - precedence matrix between elements
%      fc           - cost of elements
%   Return values:
%      sl            - schedule
%
% Complexity: Ordo(n)
% Space     : ?
%
% Reference:
% (Tkindt:mul06)
%   T'kindt et. al. Multicriteria Scheduling: Theory, Models and Algorithms, 
%   Springer, 2006,
%   Chapter 1, Lawler's algorithm for the l|prec|fmax problem
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% length checking
if length(p) ~= length(fc)
    error('Length of processing time vector must be equal to cost vector!');
end;

% precedence checking
if (size(prec,1) ~= length(p)) || (size(prec,2) ~= length(p))
    error('Size of precedence matrix is not consistent with processing time vector!');
end;

if ~isDAG(prec)
    error('Precedece matrix is not a simple (loop free, single connected) DAG!');
end;

% -- function body --

rl = []; % 'rl' means 'ready list'
sl = []; % 'sl' means 'scheduled list'

for i = length(p):-1:1
    
    % -- actualizing the 'ready list'
    % find potentially schedulable items according to updated 'prec'
    % sum(prec): summing columns -> where it equals to '0' it doesn't have precedessor
    pot = find(sum(prec,2) == 0);
    % updating 'rs': {potential items} - {scheduled items}
    rl = setdiff(pot,sl);

    if isempty(rl)
        str = strcat('Infeasible problem! There is no schedulable item at step: ',int2str(i));
        error(str);
    end;    

    % select the min cost item from the schedulable elements
    [val,ip] = min(fc(rl));
    ipp = rl(ip);
    
    sl = vertcat(ipp,sl);    % update 'scheduled list' from the last position
    
   % deleting scheduled item from 'prec', since it doesn't restrict the
   % precedented element(s) (if there is any precedented) 
   prec(:,ipp) = 0;
end;

end

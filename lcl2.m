function [sl fm] = lcl2(p,prec,f)
%LCL2 least cost last algorithm for the l|prec|fmax problem 
%
% Idea: schedule the job that is closest to being late: order the jobs by 
% nondecreasing due dates (breaking ties arbitrarily) and schedule in that order
%
% Syntax:
%     [sl fm] = lcl2(p,prec,f) 
%   Input params:
%      p             - processing times
%      prec          - precedence constraints (matrix)
%      f             - penalty functions on each job
%   Return values:
%      sl            - schedule
%      fm            - max fj(Cj)
%
% Complexity: Ordo(n^2)
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% length checking
if (length(p) ~= size(f,2)) || (length(p) ~= size(f,1))
    error('Length of processing time vector must be equal to numbers of penalty functions!');
end;

% precedence checking
if (size(prec,1) ~= length(p)) || (size(prec,2) ~= length(p))
    error('Size of precedence matrix is not consistent with processing time vector!');
end;

if ~isDAG(prec)
    error('Precedece matrix is not a simple (loop free, single connected) DAG!');
end;

% -- function body --

fm = 0;
sl = [];

rlist = []; % 'rlist' means 'ready list'
slist = []; % 'slist' means 'scheduled list'

for j=length(p):-1:1   % number of jobs
    % -- actualizing the 'ready list'
    % find potentially schedulable items according to updated 'prec'
    % sum(prec): summing columns -> where it equals to '0' it doesn't have precedessor
    pot = find(sum(prec,2) == 0);
    % updating 'rs': {potential items} - {scheduled items}
    rlist = setdiff(pot,slist);  
    
    if isempty(rlist)
        str = strcat('Infeasible problem! There is no schedulable item at step: ',int2str(j));
        error(str);
    end;
    
    % find job j that minimizes fj(p(J)) /least cost/ 
    [lc,ndx] = min(f(rlist,j));        % select least cost from rlist...
    jobndx = rlist(ndx);               % selected element
    sl = vertcat(lc,sl);               % ...and schedule it last
    fm = fm + f(jobndx,j);             % fmax
    f(jobndx,:) = inf;                 % scheduled item are practically eliminated
       
    slist = vertcat(slist,jobndx);     % update 'scheduled list'
    
   % deleting scheduled item from 'prec', since it doesn't restrict the
   % precedented element(s) (if there is any precedented) 
   prec(:,jobndx) = 0;

end;
    
end

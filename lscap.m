function [sl cmax reg] = lscap(p,m,ass,prec,schedstrategy,cap)
%LSCAP list scheduling with different strategies algorithm for the P|prec|Cmax problem 
%
% Idea: list scheduling (LS) algorithm is a generic greedy algorithm: whenever 
% a machine becomes available, process any unprocessed job
% (In contrast to P|pmtn|Cmax, P|prec|Cmax is NP-hard!)
%
% CMAX: is the lower bound approximation, namely:
% 1) CMAX >= sum(pj/m) 
% 2) CMAX >= pj for all jobs j
%
% Theorem (Gra66) LS (without any heuristic) is a 2-approximation algorithm for P|prec|Cmax.
% (i.e. Cmax  <= 2*CMAX)
%
% Syntax:
%     [sl cmax] = lscap(p,m,prec,schedstrategy,cap) 
%   Input params:
%      p             - processing times
%      m             - machine number
%      ass           - resource assignment to task
%      prec          - precedence constraints (matrix)
%      schedstrategy - heuristics for the schedule (since it is an approx. algorithm)
%      cap           - capacity (iteration velocity)
%   Return values:
%      sl            - schedule
%      cmax          - Cmax
%      reg           - is regulated (if there is at least one which can not be fitted into the 'cap')
%
% Complexity: 
%   without any heuristic:  : Ordo(n + m)
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% precedence checking
if (size(prec,1) ~= length(p)) || (size(prec,2) ~= length(p))
    error('Size of precedence matrix is not consistent with processing time vector!');
end;

if ~isDAG(prec)
    error('Precedece matrix is not a simple (loop free, single connected) DAG!');
end;

if max(ass) > m
    error('Assignment vector refers to a does not exist resource!');
end;

% -- function body --

rlist = []; % 'rlist' means 'ready list'
slist = []; % 'slist' means 'scheduled list'

% list scheduling
sl = zeros(m,length(p));  % machine x jobs
for j=1:length(p)   % number of jobs
    % -- actualizing the 'ready list'
    % find potentially schedulable items according to updated 'prec'
    % sum(prec): summing columns -> where it equals to '0' it doesn't have precedessor
    pot = find(sum(prec,2) == 0);
    % updating 'rs': {potential items} - {scheduled items}
    rlist = setdiff(pot,slist);  
    
    if isempty(rlist)
        str = strcat('Infeasible problem! There is no schedulable item at step: ',int2str(j));
        error(str);
    end;
     
    switch upper(schedstrategy)
        case 'NONE'
            jobndx = rlist(1);          % select the first element of rlist to schedule
        case 'AF'                       % schedule assigned jobs first!
            % selects the highest resource assignment first, then in the
            % end not assigned jobs (assignment value is '0') are selected
            [val,i] = max(ass(rlist));  
            jobndx = rlist(i);
        case 'LPT'
            [val,i] = max(p(rlist));
            jobndx = rlist(i);
        case 'SPT'
            [val,i] = min(p(rlist));
            jobndx = rlist(i);              
        otherwise
            error('Unknown strategy!');
    end;      
    
    % assign job to resource according to pre-assignment
    if ass(jobndx) == 0                     % there is no assignment 
            % -- find the minimally loaded machine
            [minmach,machndx] = min(sum(sl,2));        
%         % -- applying BEST FIT strategy
%            % calculate residuals
%            residual = cap-(sum(sl,2))-p(jobndx);
%            % returns indeces that has greater or equal residual than zero
%            idx0 = find(residual - p(jobndx) >= 0);
%            % returns a bin to where it is best fitted and has the
%            % lowest index (BEST FIT) 
%            [val,idx1]=min(residual(idx0));
%            % return the machine index of possible bin that is best fitted
%            machndx = idx0(idx1);
%            minmach = sum(sl(machndx,:));
    else
        machndx = ass(jobndx);
        minmach = sum(sl(machndx,:));
    end;
    
    posndx = min(find(sl(machndx,:) == 0)); % find the min unscheduled position
    if (minmach + p(jobndx)) < cap
        sl(machndx,posndx) = p(jobndx);     % schedule on machine on position
    else
        reg = true;                         % regulated (exceeds capacity)
        strcat('Not scheduled job: ',num2str(jobndx))
    end;
    
    slist = vertcat(slist,jobndx);    % update 'scheduled list'
    
   % deleting scheduled item from 'prec', since it doesn't restrict the
   % precedented element(s) (if there is any precedented) 
   prec(:,jobndx) = 0;
end;

% calculating makespan (Cmax)
cmax = max(sum(sl,2));

end

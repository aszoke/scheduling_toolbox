function [sl cmax] = esj1(pp)
%ESJ1 Johnson's algorithm for the F2|prmu|Cmax problem 
% prmu: (permutation) indicates that the operations occur on the machines in the same order
%
% Idea: We denote the operations of job j on the first and second machines as a pair (aj, bj). 
% Intuitively, we want to get jobs done on the first machine as quickly as possible so 
% as to minimize idleness on the second machine due to waiting for jobs from the first machine. 
% This suggests using an SPT rule on the first machine. On the other hand, it 
% would be useful to process the jobs with large bj as early as possible on the second machine, 
% while machine 1 is still running, so they will not create a large tail of processing 
% on machine 2 after machine 1 is finished. This suggests some kind of longest processing 
% time first (LPT) rule for machine 2.
%
% Theorem: STP1 is an exact algorithm for F2|prmu|Cmax
%
% Lemma: An instance of F2|prmu|Cmax always has an optimal schedule that is a permutation
% schedule. (Note that for three or more machines there is not necessarily an optimal permutation
% schedule.)
%
% Theorem (Joh54): Johnson's rule yields an optimal schedule for F2|prmu|Cmax.
%
% Syntax:
%     sl = ejm1(pp) 
%   Input params:
%      pp            - processing times on machine 1 and 2
%   Return values:
%      sl            - schedule
%
% Complexity: Ordo(n*log(n))
% Space     : ?
%
% Reference:
% (Tkindt:mul06)
%   T'kindt et. al. Multicriteria Scheduling: Theory, Models and Algorithms, 
%   Springer, 2006,
%   Chapter 1, Johnson's algorithm for the F2|prmu|Cmax problem
% (-:-)
%   David Karger et. al. Scheduling Algorithms
% 
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% -- function body --

sl = []; % 'sl' means 'scheduled list'

p1 = pp(1,:);
p2 = pp(2,:);


% We partition our jobs into two sets (1 and 2 sections):
% 1) items are permutated to yield a more optimized schedule
% scheduling first the job such that pi,1 < pi,2 according
% to the increasing order of the pi,1's
undx = find((p1-p2) < 0);
u = pp(:,undx);
% 2) remaining jobs are scheduled last according to the decreasing order 
% of the Pi,2's (when pi,2 > pi,1 then there will be gaps in the schedule, but 
% if we put the shortest to the end of list (decrease order) it will added to the 
% cmax value -> so it is the optimal choice)
vndx = find((p1-p2) >= 0);
v = pp(:,vndx);

us = sortrows(u',1)';     % sort by increasing values by p1
vs = sortrows(v',-2)';  % sort by decreasing values by p2

% We process jobs in this order on both machines: it is called JOHNSON'S
% RULE! (job ordering rule)
sl = horzcat(us,vs);   % concatenate not tardy and tardy jobs

% calculating makespan (Cmax)
cmax = sum(sl(1,:))+sl(2,length(sl(1,:)));

end

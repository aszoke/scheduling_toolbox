function X = lpp1(P)
%LPP1 linear programming for planning (allocation of resources) of the R|pmtn|Cmax problem 
%
% Idea: It yields assignments to resources, but does not produce schedule 
%       (operation ordering within a resource): JUST simply assign tasks to resources!
%
% min d                                                     -- minimize makespan
% s.t.
% (1) sum_i(x_{i,j})            = 1         : j = 1,2,...,n -- each job must be fully processed
% (2) sum_j(p_{i,j}*x_{i,j})   <= d         : i = 1,2,...,m -- process time on each machine no more than d time
% (3) sum_i(p_{i,j}*x_{i,j})   <= d         : j = 1,2,...,n -- no job is processed for more than d time
% (4) x_{i,j}                  >= 0         : j = 1,2,...,n and i = 1,2,...,m -- a job assigned to any machine must be non-negative
%
% where: x_{i,j} (X) -- denotes the fraction of job j that is processed on machine i
%        p_{i,j} (P) -- amount of processing that job j would require, if run entirely on machine i
%
% IMPORTANT: the above matrices in equations must be transformed into the A
% and Aeq form to produce the standard form of LP:
% min fx
% s.t.
%   A*x   <= b      -- contstraint (2) and (3) are transformed to matrix A
%   Aeq*x  = beq    -- contstraint (1) is transformed to matrix Aeq
%   x     >= a      -- contstraint (4) is expressed as lower bound
%
% Therefore matrix P(m,n) is transformed to p(m*n,1) vector (m*n variables!) so
%   (1),(2) and (3) are constructed as n,m and n equations (m+n+m equations!)
%
% Theorem (LL78) there is an exact algorithm for R|pmtn|Cmax.
%
% Syntax:
%     X = lpp1(P) 
%   Input params:
%      P             - amount of processing that job j would require, if run entirely on machine i
%   Return values:
%      X             - allocated operations (part of a job) to resources
%
% Complexity: : ?
% Space       : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms, 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: linprog

% Copyright 2006-2009

% -- input checking -- 

% -- function body --

n = size(P,2); % job number
m = size(P,1); % machine number

% maximum time to complete (optimal solution is less than the max processing time on machines)
d = max(sum(P,2));  % makespan

% -- transforming e.q. (1) to form linear equations for linprog
% produce the first coefficient matrix 

% ----------------------------------------------
% Form of the target Aeq matrix (n,nm)
%
% --m--|--m--|--....|--m--|
% 1 . 1 0 . 0 ............ = 1
% 0 . 0 1 . 1 0 0 ........ = 1
%          .......
% .................0 1 . 1 = 1
% ----------------------------------------------

A13right = zeros(n,n*m);
for i=1:n*m
    j = ceil(i/m);
    A13right(j,i)=1;
end;
Aeq = A13right;

% produce the right side of the equation 1
beq = ones(n,1);


% -- transforming e.q. (2) to form linear equations for linprog
% produce the left matrix of the second coefficient matrix 
% mutiply vertically m times

% ----------------------------------------------
% Form of the target A2 matrix (m,nm) 
%
% --------m------|-------m--------|....|----------m----|
% p(1,1) 0..... 0 p(1,m+1)  0....0 .....p(1,2m+1) 0...0 <= d
% 0 p(2,1) 0....0 0 p(2,m+1)  0..0 .....0 p(2,2m+1) 0.0 <= d
%                             .......
% 0 ...... 0p(m,1) 0... 0 p(m,m+1) .... 0...0 p(2,2m+1) <= d
% ----------------------------------------------

% transform matrix P to vector
tmp = P';
p = tmp(:)'; 

A2left = p;
for i=2:m
    A2left = vertcat(A2left,p);
end;
% produce the right matrix
A2right = zeros(m,n*m);
for h=1:m
    for i=1:n
        for j=1:m
            if j == 1
                A2right(h,(h+(i-1)*m))=1;
            end;
        end;
    end;
end;
A2 = A2left .* A2right;

% produce the right side of the equation 1
b2 = ones(m,1)*d; 

% -- transforming e.q. (3) to form linear equations for linprog
% produce the left matrix of the third coefficient matrix

% ----------------------------------------------
% Form of the target A3 matrix (n,nm) /similar to A1 but multiplied with P/
%
% --------m------|-------m--------|....|----------m----|
% p(1,1) . p(m,1) 0 ..................................0 <= d
% 0 ........... 0 p(1,2) .  p(m,2) 0 .................0 <= d
%                             .......
% ....................................0 p(1,n) . p(m,n) <= d
% ----------------------------------------------

% mutiply vertically n times
Aleft3 = p;
for i=2:n
    Aleft3 = vertcat(Aleft3,p);
end;
A3 = Aleft3 .* A13right;

% produce the right side of the equation 1
b3 = ones(n,1)*d;

% -- concatenate equations

A = vertcat(A2,A3);
b = vertcat(b2,b3);

% -- calculating the constructed linear program
% input of linprog
%     f         - Linear objective function vector f
%     A         - Matrix for linear inequality constraints
%     b         - Vector for linear inequality constraints
%     Aeq       - Matrix for linear equality constraints
%     beq       - Vector for linear equality constraints
%     lb        - Vector of lower bounds
%     ub        - Vector of upper bounds
%     x0        - Initial point for x, active set algorithm only
%     e         - Integarilty tolerance

f  = ones(n*m,1)';
lb = zeros(n*m,1)';         % it expresses the constraint (4)
ub = (ones(n*m,1) .* inf)';
options = optimset('display','off');    % optimization options structure - no output

[x,fval,exitflag] = linprog(f,A,b,Aeq,beq,lb,ub,[],options); 

if exitflag <= 0
    error('No feasible solution!');
end

% -- constructing the scheduling matrix X  
for i=1:n
    for j=1:m
        X(j,i) = x(m*(i-1)+j);
    end;
end;

end

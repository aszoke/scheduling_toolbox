function [sl sc] = srpt1(p,pr,r)
%SRTP1 shortest remaining processing time algorithm for the l|rj,pmtn|sum(Cj) problem 
%
% Note: preemptive setting, this would mean, upon the release of a job of 
% higher priority, preempting the currently running job and switching to
% the 'better' job.
%
% Idea: process the available (released) job of highest priority
%
% Theorem (Bak74): SRPT is an exact algorithm for l|rj,pmtn|sum(Cj)
%
% Syntax:
%     [sl ms] = srtp1(p,r) 
%   Input params:
%      p             - processing times
%      pr            - priorities of jobs
%      r             - release time of jobs
%   Return values:
%      sl            - schedule
%      sc            - sum(Cj)
%
% Complexity: ?
% Space     : ?
%
% Reference:
% (-:-)
%   David Karger et. al. Scheduling Algorithms
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% length checking
if length(p) ~= length(pr)
    error('Length of processing time vector must be equal to priority vector!');
elseif length(pr) ~= length(r)
    error('Length of priority vector must be equal to release time vector!');    
end;

% -- function body --

% sorting release times into ascending order
[rsorted,ndx] = sort(r,'ascend');

% sorting according to the release times order
psorted = p(ndx);
prsorted = pr(ndx);

% note: less priority more important!
sl = zeros(1,length(p));
for j=1:length(p)
    minrel = min(rsorted);          % min release value
    [tmp,col] = find(rsorted == minrel);   % indeces with min release value
    [tmp,ndx] = min(prsorted(col));  % min priority job with min release value
    sl(j) = psorted(col(ndx));           % select item in the schedule list
    prsorted(col(ndx)) = inf;  % to eliminate choosing the already selected job j
    rsorted(col(ndx)) = inf;   % detto
end;

% compute sum of completion times
sc = sum(sl(1:length(sl)));

end
